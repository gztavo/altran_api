﻿using Altran.Application.Interfaces;
using Altran.Application.Services;
using Altran.Domain.IRepository;
using Altran.Domain.Shared.Notification;
using Altran.Infra.Data.Common;
using Altran.Infra.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Altran.Infra.CrossCutting.IoC
{
    public class Injector
    {
        public static void RegisterContext(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<NotificationService>();
            services.AddScoped<IPerfilAppService, PerfilAppService>();
            services.AddScoped<IUsuarioAppService, UsuarioAppService>();
            services.AddScoped<IClaimAppService, ClaimAppService>();
        }

        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<IPerfilRepository, PerfilRepository>();
            services.AddScoped<IClaimRepository, ClaimRepository>();
        }
        public static void RegisterOptions(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Settings>(options =>
            {
                options.Seconds = int.Parse(configuration.GetSection("TokenConfigurations:Seconds").Value);
                options.Audience = configuration.GetSection("TokenConfigurations:Audience").Value;
                options.Issuer = configuration.GetSection("TokenConfigurations:Seconds").Value;
            });
        }        
    }
}
