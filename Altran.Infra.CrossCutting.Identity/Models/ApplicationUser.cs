﻿using Microsoft.AspNetCore.Identity;

namespace Altran.Infra.CrossCutting.Identity.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool UsuarioAD { get; set; }
    }


}
