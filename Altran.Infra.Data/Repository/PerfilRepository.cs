﻿using Altran.Domain.Entities.PerfilAgg;
using Altran.Domain.IRepository;
using Altran.Infra.Data.Context;
using Altran.Infra.Data.Repository.Base;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Altran.Infra.Data.Repository
{
    public class PerfilRepository : BaseRepository<Perfil>, IPerfilRepository
    {
        private readonly PostgreSqlContext _dbContext;
        public PerfilRepository(PostgreSqlContext dbContex) : base(dbContex)
        {
            _dbContext = dbContex;
        }
        
        public async Task<Perfil> ObterPerfisUsuarioPorModulo(Guid id)
        {
            return await GetAll()
                .OrderByDescending(c => c.Descricao)
                .FirstOrDefaultAsync();
        }
    }
}
