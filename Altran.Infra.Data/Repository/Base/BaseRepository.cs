﻿using System;
using System.Linq;
using Altran.Domain.IRepository.Base;
using Altran.Infra.Data.Context;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Altran.Infra.Data.Repository.Base
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly PostgreSqlContext _dbContext;

        public BaseRepository(PostgreSqlContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return GetAll().Where(predicate).AsQueryable();
        }

        public async Task<bool> Create(TEntity entity)
        {
            if (entity == null)
                return false;

            await _dbContext.Set<TEntity>().AddAsync(entity);

            var created = await _dbContext.SaveChangesAsync();

            return created > 0;
        }
        public async Task<bool> Update(Guid id, TEntity entity)
        {
            if (entity == null)
                return false;

            _dbContext.Set<TEntity>().Update(entity);

            var updated = await _dbContext.SaveChangesAsync();

            return updated > 0;
        }

        public async Task<bool> Delete(Guid id)
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);

            if (entity == null)
                return false;

            _dbContext.Set<TEntity>().Remove(entity);

            var deleted = await _dbContext.SaveChangesAsync();

            return deleted > 0;
        }
    }
}