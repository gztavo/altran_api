﻿using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.IRepository;
using Altran.Infra.Data.Context;
using Altran.Infra.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Altran.Infra.Data.Repository
{
    public class ClaimRepository : BaseRepository<Claims>, IClaimRepository
    {
        private readonly PostgreSqlContext _dbContext;
        public ClaimRepository(PostgreSqlContext dbContex) : base(dbContex)
        {
            _dbContext = dbContex;
        }

        public async Task<IEnumerable<Claims>> ObterClaimsPerfil(Guid perfis)
        {
           return await _dbContext.ClaimsPerfil.Include(i => i.Claim)
                                .Where(w => w.PerfilId == perfis)
                                .Select(c => c.Claim).ToListAsync();
        }
    }
}
