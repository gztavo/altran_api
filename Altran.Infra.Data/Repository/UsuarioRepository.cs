﻿using Altran.Domain.Entities.UsuarioAgg;
using Altran.Domain.IRepository;
using Altran.Infra.Data.Common;
using Altran.Infra.Data.Context;
using Altran.Infra.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Altran.Infra.Data.Repository
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        private readonly PostgreSqlContext _dbContext;
        public UsuarioRepository(PostgreSqlContext dbContex) : base(dbContex)
        {
            _dbContext = dbContex;
        }
    }
}
