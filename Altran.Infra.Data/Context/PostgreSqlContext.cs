﻿using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.Entities.PerfilAgg;
using Altran.Domain.Entities.UsuarioAgg;
using Microsoft.EntityFrameworkCore;

namespace Altran.Infra.Data.Context
{
    public class PostgreSqlContext: DbContext
    {
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        
        public DbSet<Claims> Claims { get; set; }
        public DbSet<Perfil> Perfis { get; set; }
        public DbSet<ClaimsPerfil> ClaimsPerfil { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
    }
}
