﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Altran.Infra.Data.Common
{
    public static class Utility
    {
        public static string GeraHash(string texto)
        {
            UnicodeEncoding Ue = new UnicodeEncoding();
            byte[] ByteSourceText = Ue.GetBytes(texto);
            MD5CryptoServiceProvider Md5 = new MD5CryptoServiceProvider();
            byte[] ByteHash = Md5.ComputeHash(ByteSourceText);
            return Convert.ToBase64String(ByteHash);
        }
    }
}
