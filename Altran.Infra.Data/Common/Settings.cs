﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Infra.Data.Common
{
    public class Settings
    {
        public int Seconds;
        public string Audience;
        public string Issuer;
    }
}
