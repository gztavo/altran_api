﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Domain.Shared.Notification
{
    public class NotificationService
    {
        public IList<NotificationItem> Notificacoes { get; private set; } = new List<NotificationItem>();

        public void AdicionarNoificacao(string chave, string valor)
        {
            Notificacoes.Add(new NotificationItem(chave, valor));
        }
    }
}
