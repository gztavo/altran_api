﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Altran.Domain.Entities.ClaimsAgg
{
    public class Claims
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Descricao { get;  set; }
        public string Tipo { get;  set; }
        public string Valor { get;  set; }
        public bool Ativo { get;  set; }

        public ICollection<ClaimsPerfil> ClaimsPerfil { get; set; }
    }
}
