﻿using Altran.Domain.Entities.PerfilAgg;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Altran.Domain.Entities.ClaimsAgg
{
    public class ClaimsPerfil
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey("Claim")]
        public Guid ClaimId { get; set; }
        
        [ForeignKey("Perfil")]
        public Guid PerfilId { get; set; }
        public bool Ativo { get;  set; }

        public Claims Claim { get; set; }
        public Perfil Perfil { get; set; }

    }
}
