﻿using Altran.Domain.Entities.PerfilAgg;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Altran.Domain.Entities.UsuarioAgg
{
    public class UsuarioPerfil
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid PerfilId { get; private set; }
        public Guid UsuarioId { get; private set; }
        public bool Ativo { get; private set; }

        [NotMapped]
        public virtual Perfil Perfil { get; private set; }

        [NotMapped]
        public virtual Usuario Usuario { get; private set; }

    }
}
