﻿using Altran.Domain.Entities.PerfilAgg;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Altran.Domain.Entities.UsuarioAgg
{
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string TokenAutenticacao { get; set; }
       
        [ForeignKey("Perfil")]
        public Guid PerfilId { get; set; }
        public bool Ativo { get; set; }

        public Perfil Perfil { get; set; }
    }
}
