﻿using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.Entities.UsuarioAgg;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Altran.Domain.Entities.PerfilAgg
{
    public class Perfil
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public bool Ativo { get; set; }

        public ICollection<ClaimsPerfil> ClaimsPerfil { get; set; }
        public ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}
