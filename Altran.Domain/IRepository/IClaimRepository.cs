﻿using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.IRepository.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Altran.Domain.IRepository
{
    public interface IClaimRepository : IBaseRepository<Claims>
    {
        Task<IEnumerable<Claims>> ObterClaimsPerfil(Guid perfis);
    }
}
