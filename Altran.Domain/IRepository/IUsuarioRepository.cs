﻿using Altran.Domain.Entities.UsuarioAgg;
using Altran.Domain.IRepository.Base;

namespace Altran.Domain.IRepository
{
    public interface IUsuarioRepository : IBaseRepository<Usuario>
    {
    }
}
