﻿using Altran.Domain.Entities.PerfilAgg;
using Altran.Domain.IRepository.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Altran.Domain.IRepository
{
    public interface IPerfilRepository : IBaseRepository<Perfil>
    {
        Task<Perfil> ObterPerfisUsuarioPorModulo(Guid id);
    }
}
