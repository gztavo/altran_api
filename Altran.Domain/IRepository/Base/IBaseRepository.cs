﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Altran.Domain.IRepository.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Get(Func<TEntity, bool> predicate);
        Task<bool> Create(TEntity entity);

        Task<bool> Update(Guid id, TEntity entity);

        Task<bool> Delete(Guid id);
    }
}
