﻿using Serilog;
using System.IO;

namespace Altran.API.Logger
{
    public class LogConfiguration
    {
        private string LogDirectoryName { get; set; }

        public LogConfiguration ConfigureDirectory(string logDirectoryName = "log", string logFileName = "LogAltran")
        {
            var robotDirectory = System.IO.Directory.GetCurrentDirectory();
            logDirectoryName = robotDirectory + "\\" + logDirectoryName + "\\";
            if (System.IO.Directory.Exists(logDirectoryName) == false)
            {
                Directory.CreateDirectory(logDirectoryName);
            }

            LogDirectoryName = logDirectoryName + logFileName + "-.txt";

            return this;
        }


        public LogConfiguration ConfigureLogger()
        {
            Serilog.Log.Logger = new LoggerConfiguration()
                                            .MinimumLevel.Debug()
                                            .Enrich.WithProperty("System", "Altran")
                                            .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({System}) {Message}{NewLine}{Exception}")
                                            .WriteTo.File(path: LogDirectoryName,
                                                          rollingInterval: RollingInterval.Day,
                                                          outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({System}) {Message}{NewLine}{Exception}")
                                            .CreateLogger();

            return this;
        }
    }
}
