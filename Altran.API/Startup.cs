using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Altran.API.Configuration;
using Altran.API.ModelExamples.User;
using Altran.Application.AutoMapper;
using Altran.Infra.CrossCutting.Identity.Authorization;
using Altran.Infra.Data.Context;
using AutoMapper;
using CorrelationId.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Events;
using Swashbuckle.AspNetCore.Filters;

namespace Altran.API
{
    public class Startup
    {
        private const string SecretKey = "ChaveSegurancaAltran";
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        private const string appSettingsSection = "AltranSettings:";

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PostgreSqlContext>(
                   options => options.UseNpgsql(
                       Configuration.GetConnectionString("AltranDB")));

            services.AddCorrelationId();

            services.AddSingleton(_signingKey);

            var file = Configuration["LogConfiguration:LogDirectoryName"] + "\\" + Configuration["LogConfiguration:LogFileName"] + ".txt";

            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
               .Enrich.FromLogContext()
               .Enrich.WithProperty("Sistema", "Altran")
               .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({Sistema}) ({Operacao}) (X-Correlation-ID: {CorrelationID})  (UserId: {UserId}) {Message}{NewLine}{Exception}{NewLine}")
               .WriteTo.File(path: file,
                             rollingInterval: RollingInterval.Day,
                             outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({Sistema}) ({Operacao}) (X-Correlation-ID: {CorrelationID}) (UserId: {UserId}) {Message}{NewLine}{Exception}{NewLine}")
               .CreateLogger();

            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Início Configuração 'ConfigureServices'.");

            services.AddCors();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddMvc(option =>
            {
                option.EnableEndpointRouting = false;
                var policy = new AuthorizationPolicyBuilder()
                             .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                             .RequireAuthenticatedUser()
                             .Build();
                option.Filters.Add(new AuthorizeFilter(policy));
            }
            );
            #region Authorization/Authentication
            var jwtAppSettingOptions = this.Configuration.GetSection(nameof(JwtTokenOptions));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtTokenOptions.Issuer)],
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddAuthorizationService();

            #endregion

            services.AddDIConfiguration(Configuration);


            #region Swagger
            services.AddSwaggerGen(options =>
            {
                options.ExampleFilters();

                string basePath = PlatformServices.Default.Application.ApplicationBasePath;
                string moduleName = GetType().GetTypeInfo().Module.Name.Replace(".dll", ".xml");
                string filePath = Path.Combine(basePath, moduleName);

                options.IncludeXmlComments(filePath);

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });


                string readme = File.ReadAllText(Path.Combine(basePath, "README.md"));

                OpenApiInfo info = Configuration.GetSection("Info").Get<OpenApiInfo>();
                options.SwaggerDoc(info.Version, new OpenApiInfo { Description = readme, Title = "API Altran", Version = info.Version });

            });
            #endregion

            services.AddSwaggerExamplesFromAssemblyOf<UserViewModelGetExample>();
            #region AutoMapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainToViewModelMappingProfile());
                mc.AddProfile(new ViewModelToDomainMappingProfile());
                mc.AllowNullCollections = true;
                mc.AllowNullDestinationValues = true;
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion


            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Fim Configuração 'ConfigureServices': " + Configuration["Constantes:System"]);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Início Configuração 'Configure': " + Configuration["Constantes:System"]);


            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseCors(builder => builder
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader());

            app.UseMvc();

            #region Swagger

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{httpReq.PathBase}" } };
                });
            });

            app.UseSwaggerUI(c =>
            {
                var swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "Altran - API");
            });
            app.UseRouting();
            app.UseEndpoints(x => x.MapControllers());

            #endregion
            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Fim Configuração 'Configure': " + Configuration["Constantes:System"]);
        }
    }
}
