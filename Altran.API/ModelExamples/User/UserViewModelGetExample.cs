﻿using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Bogus;
using Swashbuckle.AspNetCore.Filters;

namespace Altran.API.ModelExamples.User
{
    public class UserViewModelGetExample : IExamplesProvider<LoginViewModel>
    {
        public LoginViewModel GetExamples()
        {
            var consulta = new Faker<LoginViewModel>()
               .RuleFor(r => r.Name, fr => "Descrição Name")
               .RuleFor(r => r.Password, fr => "Descrição Password");

            return consulta;

        }
    }
}
