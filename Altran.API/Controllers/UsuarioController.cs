﻿using System;
using System.Threading.Tasks;
using Altran.API.Configuration;
using Altran.Application.Interfaces;
using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Domain.Shared.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Altran.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class UsuarioController : BaseController
    {
        private readonly IUsuarioAppService _usuarioAppService;
        private readonly IPerfilAppService _perfilAppService;
        public UsuarioController(NotificationService _notificationService,
                                 IUsuarioAppService usuarioAppService,
                                 IPerfilAppService perfilAppService) : base(_notificationService)
        {
           
            _usuarioAppService = usuarioAppService;
            _perfilAppService = perfilAppService;


        }
        /// <summary>
        /// Obter usuário por Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = UsuariosAltranClaim.ConsultarUsuario)]
        [Route("ObterPorId/{id}")]
        public ActionResult<UsuarioViewModel> Get(Guid id)
        {
            var result = _usuarioAppService.GetById(id);
            
            if (result == null)
            {
                NotificarErro("02", "Não foi encontrado nenhum usuário");
                return Response(404);
            }
            return Response(200, result);
        }

        /// <summary>
        ///  Inserir Usuário.
        /// </summary>
        /// <param name="usuarioViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Inserir")]
        [Authorize(Policy = UsuariosAltranClaim.InserirUsuario)]
        public async Task<IActionResult> Post([FromBody]InserirUsuarioViewModel usuarioViewModel)
        {
           var usarioValidacaoEmail = _usuarioAppService.ObterUsuarioPeloEmail(usuarioViewModel.Email);
           if (usarioValidacaoEmail != null)
           {
               NotificarErro("01", $"O usuário informado '{usuarioViewModel.Name}' já existe!");
               return Response(400);
           }
           
            var validacaoPerfil = _perfilAppService.GetById(usuarioViewModel.PerfilId);
            if (validacaoPerfil == null)
            {
                NotificarErro("01", $"O perfil informado '{usuarioViewModel.PerfilId}' não existe!");
                return Response(400);
            }

            var result = await _usuarioAppService.Insert(usuarioViewModel);
            usuarioViewModel.Name = usuarioViewModel.Name.ToUpper();
            usuarioViewModel.Email = usuarioViewModel.Email.ToUpper();
            if (result == false)
            {
                return StatusCode(400);
            }
            return Response(201, usuarioViewModel);
        }

        /// <summary>
        /// Atualizar Usuário.
        /// </summary>
        /// <param name="usuarioViewModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Atualizar")]
        [Authorize(Policy = UsuariosAltranClaim.AtualizarUsuario)]
        public async Task<IActionResult> Put([FromBody] UsuarioViewModel usuarioViewModel)
        {
            var usuarioExiste = _usuarioAppService.GetById(usuarioViewModel.Id);
            if (usuarioExiste == null)
            {
                NotificarErro("02", "Não foi encontrado nenhum usuário");
                return Response(404);
            }

            var result = await _usuarioAppService.Update(usuarioViewModel);
            if (result == false)
            {
                NotificarErro("00", $"Operação não realizada!");
                return Response(400);
            }
            return Response(200, usuarioViewModel);
        }

        /// <summary>
        /// Deletar Usuário.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Deletar")]
        [Authorize(Policy = UsuariosAltranClaim.DeletarUsuario)]
        public async Task<ActionResult<UsuarioViewModel>> Delete(Guid id)
        {
            var usuarioExiste = _usuarioAppService.GetById(id);
            if (usuarioExiste == null)
            {
                NotificarErro("02", "Não foi encontrado nenhum usuário");
                return Response(404);
            }

            var result = await _usuarioAppService.Delete(id);
            if (result == false)
            {
                NotificarErro("00", $"Operação não realizada!");
                return Response(400);
            }
            return Response(200, $"Usuário {usuarioExiste.Name}, deletado com sucesso.");
        }
    }
}