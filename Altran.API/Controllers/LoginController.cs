﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Altran.API.ModelExamples.User;
using Altran.Application.Interfaces;
using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.Shared.Notification;
using Altran.Infra.CrossCutting.Identity.Authorization;
using Altran.Infra.CrossCutting.Identity.Models;
using Altran.Infra.Data.Common;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Filters;

namespace Altran.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class LoginController : BaseController
    {
        private readonly JwtTokenOptions _jwtTokenOptions;
        private readonly IUsuarioAppService _usuarioAppService;
        private readonly IPerfilAppService _perfilAppService;
        private readonly IClaimAppService _claimAppService;
        private readonly IMapper _mapper;
        private Settings _settings;
        public LoginController(NotificationService _notificationService,
                               IOptions<JwtTokenOptions> jwtTokenOptions,
                               IUsuarioAppService usuarioAppService,
                               IPerfilAppService perfilAppService,
                               IMapper mapper,
                               IClaimAppService claimAppService, IOptions<Settings> settings) : base(_notificationService)
        {
            _jwtTokenOptions = jwtTokenOptions.Value;
            _mapper = mapper;
            _usuarioAppService = usuarioAppService;
            _perfilAppService = perfilAppService;
            _claimAppService = claimAppService;
            _settings = settings.Value;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        [SwaggerResponseExample((int)HttpStatusCode.OK, typeof(UserViewModelGetExample))]
        public async Task<ActionResult> Login([FromBody]LoginViewModel model)
        {
            var resultUsuario = _usuarioAppService.ObterUsuario(model);
            if (resultUsuario == null)
            {
                NotificarErro("01", "Usuário ou senha incorretos");
                return Response(401);
            }

            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, resultUsuario.Name));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, await _jwtTokenOptions.JtiGenerator()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtTokenOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64));
            claims.Add(new Claim("nome", resultUsuario.Name));

            var claimsPerfis = new List<Claims>();
            var perfis = _perfilAppService.ObterPerfisUsuarioPorModulo(resultUsuario.PerfilId).FirstOrDefault();
            claimsPerfis = _mapper.Map<List<Claims>>(await _claimAppService.ObterClaimsPerfil(perfis.Id));

            if (claimsPerfis != null && claimsPerfis.Any())
                claims.AddRange(claimsPerfis.Select(c => new Claim(c.Tipo, c.Valor)).ToList());

            var identity = new ClaimsIdentity(claims);
            var dataCriacao = DateTime.Now;
            var dataExpiracao = dataCriacao + TimeSpan.FromSeconds(_settings.Seconds);
            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _settings.Issuer,
                Audience = _settings.Audience,
                SigningCredentials = _jwtTokenOptions.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao,
            });

            var token = handler.WriteToken(securityToken);
            resultUsuario.TokenAutenticacao = token;

            var response = new
            {
                authenticated = true,
                created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                accessToken = token,
                message = "OK"
            };
            return Response(200, resultUsuario);
        }
        private static long ToUnixEpochDate(DateTime date) => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}