﻿using System;
using System.Collections.Generic;
using System.Net;
using Altran.API.Configuration;
using Altran.Application.Interfaces;
using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using Altran.Domain.Shared.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Altran.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerfilController : BaseController
    {
        IPerfilAppService _PerfilAppService;

        public PerfilController(NotificationService _notificationService,
                                    IPerfilAppService PerfilAppService) : base(_notificationService)
        {
            _PerfilAppService = PerfilAppService;
        }
        /// <summary>
        /// Obter todos os perfis.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ListarTodos")]
        [AllowAnonymous]
        [SwaggerResponseExample((int)HttpStatusCode.OK, typeof(PerfilViewModel))]
        public ActionResult<IEnumerable<PerfilViewModel>> Listar()
        {
            var result = _PerfilAppService.GetAll();
            if (result == null)
            {
                NotificarErro("02", "Não foi encontrada nenhuma Perfil");
                return Response(404);
            }
            return Response(200, result);
        }
        /// <summary>
        /// Obter perfil por Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("ObterPorId/{id}")]
        [SwaggerResponseExample((int)HttpStatusCode.OK, typeof(PerfilViewModel))]
        [Authorize(Policy = UsuariosAltranClaim.ConsultarUsuario)]
        public ActionResult<PerfilViewModel> ObterPorId(Guid id)
        {
            var result = _PerfilAppService.GetById(id);
            if (result == null)
            {
                NotificarErro("02", "Não foi encontrada nenhuma Perfil com o ID informado");
                return Response(404);
            }
            return Response(200, result);
        }
    }
}