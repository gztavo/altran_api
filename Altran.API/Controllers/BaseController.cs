﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Altran.Domain.Shared.Notification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Altran.API.Controllers
{
    public class BaseController : Controller
    {
        private readonly NotificationService _notificationService;
        public BaseController(NotificationService notificationService)
        {
            _notificationService = notificationService;
        }
        protected ActionResult Response(int statusCode, object result = null)
        {
            return StatusCode(statusCode, new { success = OperacaoValida(), data = result, errors = _notificationService.Notificacoes.Select(x => new { x.Chave, x.Valor }) });
        }
        protected void NotificarErro(string codigo, string mensagem)
        {
            _notificationService.AdicionarNoificacao(codigo, mensagem);
        }
        protected bool OperacaoValida()
        {
            return !_notificationService.Notificacoes.Any();
        }
    }
}