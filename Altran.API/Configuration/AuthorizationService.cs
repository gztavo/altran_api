﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Altran.API.Configuration
{
    public static class AuthorizationService
    {
        public static void AddAuthorizationService(this IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.OutputFormatters.Remove(new XmlDataContractSerializerOutputFormatter());
                options.UseCentralRoutePrefix(new RouteAttribute("api/"));

                var policy = new AuthorizationPolicyBuilder()
                 .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                 .RequireAuthenticatedUser()
                 .Build();

                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy(UsuariosAltranClaim.ConsultarUsuario, policy => policy.RequireClaim(UsuariosAltranClaim.UnidadesAltran, UsuariosAltranClaim.ConsultarUsuario));
                auth.AddPolicy(UsuariosAltranClaim.InserirUsuario, policy => policy.RequireClaim(UsuariosAltranClaim.UnidadesAltran, UsuariosAltranClaim.InserirUsuario));
                auth.AddPolicy(UsuariosAltranClaim.AtualizarUsuario, policy => policy.RequireClaim(UsuariosAltranClaim.UnidadesAltran, UsuariosAltranClaim.AtualizarUsuario)); 
                auth.AddPolicy(UsuariosAltranClaim.DeletarUsuario, policy => policy.RequireClaim(UsuariosAltranClaim.UnidadesAltran, UsuariosAltranClaim.DeletarUsuario));
            });
        }
    }
    public class RouteConvention : IApplicationModelConvention
    {
        private readonly AttributeRouteModel _centralPrefix;

        public RouteConvention(IRouteTemplateProvider routeTemplateProvider)
        {
            _centralPrefix = new AttributeRouteModel(routeTemplateProvider);
        }

        public void Apply(ApplicationModel application)
        {
            foreach (var controller in application.Controllers)
            {
                var matchedSelectors = controller.Selectors.Where(x => x.AttributeRouteModel != null).ToList();
                if (matchedSelectors.Any())
                {
                    foreach (var selectorModel in matchedSelectors)
                    {
                        selectorModel.AttributeRouteModel = AttributeRouteModel.CombineAttributeRouteModel(_centralPrefix,
                            selectorModel.AttributeRouteModel);
                    }
                }

                var unmatchedSelectors = controller.Selectors.Where(x => x.AttributeRouteModel == null).ToList();
                if (unmatchedSelectors.Any())
                {
                    foreach (var selectorModel in unmatchedSelectors)
                    {
                        selectorModel.AttributeRouteModel = _centralPrefix;
                    }
                }
            }
        }
    }

    public static class MvcOptionsExtensions
    {
        public static void UseCentralRoutePrefix(this MvcOptions opts, IRouteTemplateProvider routeAttribute)
        {
            opts.Conventions.Insert(0, new RouteConvention(routeAttribute));
        }
    }
    internal class UsuariosAltranClaim
    {
        internal const string UnidadesAltran = "Usuarios";
        internal const string ConsultarUsuario = "ConsultarUsuario";
        internal const string InserirUsuario = "InserirUsuario";
        internal const string AtualizarUsuario = "AtualizarUsuario";
        internal const string DeletarUsuario = "DeletarUsuario";
    }
}
