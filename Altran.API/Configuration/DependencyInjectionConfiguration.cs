﻿using Altran.Infra.CrossCutting.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Altran.API.Configuration
{
    public static class DependencyInjectionConfiguration
    {
        public static void AddDIConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            Injector.RegisterContext(services, configuration);
            Injector.RegisterServices(services, configuration);
            Injector.RegisterRepository(services);
            Injector.RegisterOptions(services, configuration);
        }
    }
}
