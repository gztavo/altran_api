﻿using Altran.Application.ViewModel.ClaimsViewModel;
using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.Entities.PerfilAgg;
using Altran.Domain.Entities.UsuarioAgg;
using AutoMapper;

namespace Altran.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ClaimPerfilViewModel, ClaimsPerfil>();
            CreateMap<ClaimViewModel, Claims>();
            CreateMap<PerfilViewModel, Perfil>();
            CreateMap<UsuarioViewModel, Usuario>();
            CreateMap<InserirUsuarioViewModel, Usuario>();
            CreateMap<LoginViewModel, Usuario>();
            CreateMap<LoginViewModel, UsuarioViewModel>();
        }
    }
}
