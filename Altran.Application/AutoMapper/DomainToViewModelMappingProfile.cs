﻿using Altran.Application.ViewModel.ClaimsViewModel;
using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.Entities.PerfilAgg;
using Altran.Domain.Entities.UsuarioAgg;
using AutoMapper;

namespace Altran.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<ClaimsPerfil, ClaimPerfilViewModel>();
            CreateMap<Claims, ClaimViewModel>();
            CreateMap<Perfil, PerfilViewModel>();
            CreateMap<Usuario, LoginViewModel>();
            CreateMap<Usuario, UsuarioViewModel>();
            CreateMap<Usuario, InserirUsuarioViewModel>();
            CreateMap<UsuarioViewModel, LoginViewModel>();
        }
    }
}
