﻿using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using System;

namespace Altran.Application.ViewModel.ClaimsViewModel
{
    public class ClaimPerfilViewModel
    {
        public Guid Id { get; set; }
        public Guid ClaimId { get; private set; }

        public Guid PerfilId { get; private set; }

        public bool Ativo { get; private set; }

        public virtual ClaimViewModel Claim { get; private set; }

        public virtual PerfilViewModel Perfil { get; private set; }

    }
}
