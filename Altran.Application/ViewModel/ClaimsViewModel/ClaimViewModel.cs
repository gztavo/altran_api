﻿using System.Collections.Generic;

namespace Altran.Application.ViewModel.ClaimsViewModel
{
    public class ClaimViewModel
    {
        public string Descricao { get;  set; }
        public string Tipo { get;  set; }
        public string Valor { get;  set; }
        public bool Ativo { get;  set; }
        public virtual ICollection<ClaimPerfilViewModel> ClaimsPerfil { get; set; }
    }
}
