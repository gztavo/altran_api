﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Altran.Application.ViewModel.UsuarioViewModel
{
    public class UsuarioPerfilViewModel
    {
        public int PerfilId { get; private set; }

        public int UsuarioId { get; private set; }

        public bool Ativo { get; private set; }

        [NotMapped]
        public virtual Altran.Domain.Entities.PerfilAgg.Perfil Perfil { get; private set; }

        [NotMapped]
        public virtual Altran.Domain.Entities.UsuarioAgg.Usuario Usuario { get; private set; }
    }
}
