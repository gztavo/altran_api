﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Application.ViewModel.Usuario.UsuarioViewModel
{
    public class LoginViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
