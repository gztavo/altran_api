﻿using System;

namespace Altran.Application.ViewModel.Usuario.UsuarioViewModel
{
    public class UsuarioViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Guid PerfilId { get; set; }
        public string TokenAutenticacao { get; set; }
    }
}
