﻿using System;

namespace Altran.Application.ViewModel.UsuarioViewModel
{
    public class AtualizarUsuarioViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Guid PerfilId { get; set; }
        public bool Ativo { get; set; }
    }
}
