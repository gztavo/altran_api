﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Application.ViewModel.UsuarioViewModel
{
    public class InserirUsuarioViewModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Guid PerfilId { get; set; }
        public bool Ativo { get; set; }
    }
}
