﻿using Altran.Application.ViewModel.ClaimsViewModel;
using System;
using System.Collections.Generic;

namespace Altran.Application.ViewModel.Perfil.PerfilViewModel
{
    public class PerfilViewModel
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<ClaimPerfilViewModel> ClaimsPerfil { get; private set; }
        public virtual ICollection<ClaimPerfilViewModel> UsuarioPerfil { get; private set; }
    }
}
