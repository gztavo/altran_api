﻿using Altran.Application.Interfaces;
using Altran.Domain.Entities.ClaimsAgg;
using Altran.Domain.IRepository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Altran.Application.Services
{
    public class ClaimAppService : IClaimAppService
    {
        private readonly IClaimRepository _claimRepository;
        private readonly IMapper _mapper;

        public ClaimAppService(IClaimRepository claimRepository,
                              IMapper mapper)
        {
            _claimRepository = claimRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Claims>> ObterClaimsPerfil(Guid perfis)
        {
            return _mapper.Map<List<Claims>>(await _claimRepository.ObterClaimsPerfil(perfis));
        }
    }
}
