﻿using Altran.Application.Interfaces;
using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using Altran.Domain.IRepository;
using AutoMapper;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Altran.Application.Services
{
    public class PerfilAppService : IPerfilAppService
    {
        private readonly IPerfilRepository _perfilRepository;
        private readonly IMapper _mapper;

        public PerfilAppService(IPerfilRepository perfilRepository,
                              IMapper mapper)
        {
            _perfilRepository = perfilRepository;
            _mapper = mapper;
        }


        public IEnumerable<PerfilViewModel> GetAll()
        {
            var caregoriaResult = _perfilRepository.GetAll();
            var viewModel = _mapper.Map<IEnumerable<PerfilViewModel>>(caregoriaResult);
            return viewModel;
        }

        public PerfilViewModel GetById(Guid id)
        {
            var perfilResult = _perfilRepository.Get(m => m.Id == id).FirstOrDefault();
            var viewModel = _mapper.Map<PerfilViewModel>(perfilResult);
            return viewModel;
        }

        public IEnumerable<PerfilViewModel> ObterPerfisUsuarioPorModulo(Guid id)
        {
            var perfilResult = _perfilRepository.Get(m => m.Id == id);
            var viewModel = _mapper.Map<IEnumerable<PerfilViewModel>>(perfilResult);
            return viewModel;
        }
    }
}
