﻿using Altran.Application.Interfaces;
using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Domain.Entities.UsuarioAgg;
using Altran.Domain.IRepository;
using Altran.Infra.Data.Common;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Query.ExpressionTranslators.Internal;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Altran.Application.Services
{
    public class UsuarioAppService : IUsuarioAppService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMapper _mapper;

        public UsuarioAppService(IUsuarioRepository usuarioRepository,
                              IMapper mapper)
        {
            _usuarioRepository = usuarioRepository;
            _mapper = mapper;
        }

        public UsuarioViewModel ObterUsuario(LoginViewModel loginViewModel)
        {
            var usuario = _mapper.Map<Usuario>(loginViewModel);
            usuario.Password = Utility.GeraHash(usuario.Password);
            var resultUsuario = _usuarioRepository.Get(c => c.Name == loginViewModel.Name && 
                                                           c.Password == usuario.Password).FirstOrDefault();
            resultUsuario.Password = "";
           var result = _mapper.Map<UsuarioViewModel>(resultUsuario);
           return result;
        }
        public async Task<bool> Insert(InserirUsuarioViewModel viewModel)
        {
            try
            {
                var usuario = _mapper.Map<Usuario>(viewModel);
                usuario.Password = Utility.GeraHash(viewModel.Password);
                var result = await _usuarioRepository.Create(usuario);
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<bool> Update(UsuarioViewModel usuarioVM)
        {
            try
            {
                var usuario = _mapper.Map<Usuario>(usuarioVM);
                var result = await _usuarioRepository.Update(usuario.Id, usuario);
                return result;
            }
            catch (Exception)
            {
                return false;
            }
           
        }
        public async Task<bool> Delete(Guid id)
        {
            try
            {
                var result = await _usuarioRepository.Delete(id);
                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public UsuarioViewModel GetById(Guid id)
        {
            var usuario = new Usuario { Id = id };
            var usuarioResult = _usuarioRepository.Get(u => u.Id == id).FirstOrDefault();
            usuarioResult.Password = "";
            var viewModel = _mapper.Map<UsuarioViewModel>(usuarioResult);
            return viewModel;
        }
        public UsuarioViewModel ObterUsuarioPeloEmail(string email)
        {
            var usuarioResult = _usuarioRepository.Get(c => c.Email == email).FirstOrDefault();
            var viewModel = _mapper.Map<UsuarioViewModel>(usuarioResult);
            return viewModel;
        }
    }
}
