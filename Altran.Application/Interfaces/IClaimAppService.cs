﻿using Altran.Application.ViewModel.ClaimsViewModel;
using Altran.Domain.Entities.ClaimsAgg;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Altran.Application.Interfaces
{
    public interface IClaimAppService
    {
        Task<IEnumerable<Claims>> ObterClaimsPerfil(Guid perfis);
    }
}
