﻿using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using System;
using System.Threading.Tasks;

namespace Altran.Application.Interfaces
{
    public interface IUsuarioAppService
    {
        Task<bool> Insert(InserirUsuarioViewModel usuario);
        Task<bool> Delete(Guid id);
        Task<bool> Update(UsuarioViewModel usuarioVM);
        UsuarioViewModel ObterUsuario(LoginViewModel loginViewModel);
        UsuarioViewModel GetById(Guid id);
        UsuarioViewModel ObterUsuarioPeloEmail(string email);
    }
}
