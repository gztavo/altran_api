﻿using Altran.Application.ViewModel.Perfil.PerfilViewModel;
using System;
using System.Collections.Generic;

namespace Altran.Application.Interfaces
{
    public interface IPerfilAppService
    {
        IEnumerable<PerfilViewModel> GetAll();
        IEnumerable<PerfilViewModel> ObterPerfisUsuarioPorModulo(Guid id);
        PerfilViewModel GetById(Guid id);
    }
}
