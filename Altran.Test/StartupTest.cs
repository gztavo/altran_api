﻿using Altran.API.Configuration;
using Altran.Application.AutoMapper;
using Altran.Infra.Data.Context;
using AutoMapper;
using CorrelationId.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;

namespace Altran.Test
{
    public class StartupTest
    {
        public StartupTest(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PostgreSqlContext>(
                   options => options.UseNpgsql(
                       Configuration.GetConnectionString("AltranDB")));

            services.AddCorrelationId();

            var file = Configuration["LogConfiguration:LogDirectoryName"] + "\\" + Configuration["LogConfiguration:LogFileName"] + ".txt";

            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
               .Enrich.FromLogContext()
               .Enrich.WithProperty("Sistema", "Altran")
               .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({Sistema}) ({Operacao}) (X-Correlation-ID: {CorrelationID})  (UserId: {UserId}) {Message}{NewLine}{Exception}{NewLine}")
               .WriteTo.File(path: file,
                             rollingInterval: RollingInterval.Day,
                             outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] ({Sistema}) ({Operacao}) (X-Correlation-ID: {CorrelationID}) (UserId: {UserId}) {Message}{NewLine}{Exception}{NewLine}")
               .CreateLogger();

            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Início Configuração 'ConfigureServices'.");

            services.AddCors();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddDIConfiguration(Configuration);


            #region AutoMapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainToViewModelMappingProfile());
                mc.AddProfile(new ViewModelToDomainMappingProfile());
                mc.AllowNullCollections = true;
                mc.AllowNullDestinationValues = true;
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion


            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Fim Configuração 'ConfigureServices': " + Configuration["Constantes:System"]);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Início Configuração 'Configure': " + Configuration["Constantes:System"]);


            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseCors(builder => builder
             .AllowAnyOrigin()
             .AllowAnyMethod()
             .AllowAnyHeader());
            
            app.UseRouting();
            app.UseEndpoints(x => x.MapControllers());
            Serilog.Log.ForContext("Operacao", "Configuracao", destructureObjects: true)
                       .Information("Fim Configuração 'Configure': " + Configuration["Constantes:System"]);
        }
    }
}
