﻿using Altran.Application.AutoMapper;
using Altran.Test.Fixture;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Test.Ultis.Mapper
{
    public class MapperTestBuilder : BaseBuilder<MapperTestBuilder, IMapper>
    {
        public MapperTestBuilder()
        {
            var configuration = new MapperConfiguration(x =>
            {
                x.AddProfile(new DomainToViewModelMappingProfile());
                x.AddProfile(new ViewModelToDomainMappingProfile());
                x.AllowNullDestinationValues = true;
                x.AllowNullCollections = true;
            });

            Model = configuration.CreateMapper();
        }
    }
}
