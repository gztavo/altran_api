﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Test.Fixture
{
    public abstract class BaseBuilder<T, M>
    {
        protected M Model;

        public static implicit operator M(BaseBuilder<T, M> instance)
        {
            return instance.Build();
        }

        public M Build()
        {
            return Model;
        }
    }
}
