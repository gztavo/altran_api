﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Net.Http;

namespace Altran.Test.Fixture
{
    public class BaseTestFixture : IDisposable
    {
        private readonly TestServer Server;
        public WebHostBuilder _builder { get; }
        public HttpClient Client { get; }
        public BaseTestFixture()
        {
            var builder = new WebHostBuilder()
               .UseStartup<StartupTest>()
               .ConfigureAppConfiguration(config =>
               {
                   config.SetBasePath(Directory.GetCurrentDirectory());
                   config.AddJsonFile("appsettings.json", reloadOnChange: true, optional: false);
                   config.Build();
               });

            Server = new TestServer(builder);

            Client = Server.CreateClient();
        }

        public void Dispose()
        {
            Client.Dispose();
            Server.Dispose();
        }
    }
}
