﻿using Altran.Application.ViewModel.Usuario.UsuarioViewModel;
using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Test.Fixture;
using System;
using System.Collections.Generic;
using System.Text;

namespace Altran.Test.Model
{
    public class UsuarioViewModelTestBuilder : BaseBuilder<UsuarioViewModelTestBuilder, UsuarioViewModel>
    {
        public UsuarioViewModelTestBuilder()
        {
            Model = new UsuarioViewModel();
        }

        public UsuarioViewModelTestBuilder Default()
        {
            Model.Id = Guid.Parse("1c041af5-24e4-4a2f-9ad2-8b6cd144556e");

            return this;
        }
    }
}
