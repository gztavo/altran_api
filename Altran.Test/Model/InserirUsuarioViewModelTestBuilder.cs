﻿using Altran.Application.ViewModel.UsuarioViewModel;
using Altran.Test.Fixture;
using Newtonsoft.Json;
using System.IO;

namespace Altran.Test.Model
{
    public class InserirUsuarioViewModelTestBuilder : BaseBuilder<InserirUsuarioViewModelTestBuilder, InserirUsuarioViewModel>
    {
        public InserirUsuarioViewModelTestBuilder()
        {
            Model = new InserirUsuarioViewModel();
        }

        public InserirUsuarioViewModelTestBuilder Default()
        {
            Model = JsonConvert.DeserializeObject<InserirUsuarioViewModel>(Usuario);

            return this;
        }

        public string Usuario
        {
            get
            {
                using (StreamReader r = new StreamReader("Utils/JsonHelper/Usuario.json"))
                {
                    return r.ReadToEnd();
                }
            }
        }

    }
}
