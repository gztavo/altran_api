﻿using Altran.Application.Interfaces;
using Altran.Application.Services;
using Altran.Infra.CrossCutting.IoC;
using Altran.Infra.Data.Context;
using Altran.Infra.Data.Repository;
using Altran.Test.Fixture;
using Altran.Test.Model;
using Altran.Test.Ultis;
using Altran.Test.Ultis.Mapper;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Xunit;
using Xunit.Abstractions;

namespace Altran.Test.Scenario.UsuarioAggTest
{
    public class UsuarioTest : IClassFixture<BaseTestFixture>
    {
        #region Private
        private readonly ITestOutputHelper _output;
        private readonly IConfiguration _configuration;
        private readonly IServiceCollection _services;
        private readonly IUsuarioAppService _usuarioAppService;
        private readonly PostgreSqlContext _dbContext;
        IMapper _mapper = new MapperTestBuilder().Build();
        #endregion

        #region Construtor
        public UsuarioTest(ITestOutputHelper output)
        {
            _output = output;
            var dbContext = new DbContextOptionsBuilder<PostgreSqlContext>().UseNpgsql("Server=localhost;Port=15432;Database=postgres;User Id=postgres;Password=altran!;CommandTimeout=20");
            _dbContext = new PostgreSqlContext(dbContext.Options);
            _configuration = TestHelper.GetConfiguration();
            _usuarioAppService = new UsuarioAppService(new UsuarioRepository(_dbContext), _mapper);

            _services = new ServiceCollection();
            Injector.RegisterContext(_services, _configuration);
            Injector.RegisterServices(_services, _configuration);
            Injector.RegisterRepository(_services);
        }
        #endregion

        #region Obter Usuario por Id
        [Fact]
        public void ObterUsuarioId()
        {
            var usuario = new UsuarioViewModelTestBuilder().Default().Build();
            var resultUsuario =  _usuarioAppService.GetById(usuario.Id);
            Assert.NotNull(resultUsuario);
        }
        #endregion

        #region Inserir Usuario 
        [Fact]
        public async void InserirUsuario()
        {
            var usuario = new InserirUsuarioViewModelTestBuilder().Default().Build();
            var resultUsuario = await _usuarioAppService.Insert(usuario);
            Assert.NotNull(resultUsuario);
        }
        #endregion
    }
}
