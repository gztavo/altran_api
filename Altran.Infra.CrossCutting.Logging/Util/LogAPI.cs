﻿using CorrelationId.Abstractions;
using System;

namespace Altran.Infra.CrossCutting.Logging.Util
{
    public class LogAPI
    {
        ICorrelationContextAccessor _correlationContextAccessor;

        public LogAPI(ICorrelationContextAccessor correlationContextAccessor)
        {
            _correlationContextAccessor = correlationContextAccessor;
        }

        public static void LogarInformacao(string informacao)
        {
            Serilog.Log.ForContext("Operacao", "Processamento", destructureObjects: true)
                       .Information(informacao);
        }

        public static void LogarErro(Exception exception, string erroDescricao)
        {
            Serilog.Log.ForContext("Operacao", "Processamento", destructureObjects: true)
                       .Error(exception, erroDescricao);
        }
    }
}
